extends Panel

const DATABASE_CONFIG_FILE_NAME = "godot_db.json"

const TABLE_NAME_LABEL = preload("res://src/scenes/TableNameLabel.tscn")

onready var file_dialog : FileDialog
onready var database_path_lineedit : LineEdit
onready var create_database_dialog : AcceptDialog
onready var tables_list_container : ItemList
onready var table_fields_container : ItemList
onready var new_table_dialog : ConfirmationDialog
onready var new_table_line_edit : LineEdit
onready var new_field_dialog : ConfirmationDialog
onready var new_field_line_edit : LineEdit
onready var quit_dialog : AcceptDialog
onready var autosave_check_box : CheckBox

var database_dir : Directory
var current_config_file_path : String

var database_instance : GodotDB

var current_tables_list_index : int
var current_fields_list_index : int


func _ready():
	file_dialog = find_node("FileDialog")
	database_path_lineedit = find_node("DatabasePathLineEdit")
	create_database_dialog = find_node("CreateDatabaseDialog")
	tables_list_container = find_node("TablesListItemsList")
	table_fields_container = find_node("TableFieldsItemsList") 
	new_table_dialog = find_node("NewTableDialog")
	new_table_line_edit = find_node("NewTableLineEdit")
	new_field_dialog = find_node("NewFieldDialog")
	new_field_line_edit = find_node("NewFieldLineEdit")
	quit_dialog = find_node("QuitDialog")
	autosave_check_box = find_node("AutosaveCheckBox")


func create_new_database_structure():
	database_instance = GodotDB.new()


func create_db_config_file():
	var db_config_file = open_db_config_file(File.WRITE)
	#db_config_file.store_string("GODOT_DB")
	db_config_file.close()
	print("New database file created")


func is_dirpath_empty():
	database_dir.list_dir_begin(true, true)
	var filename = database_dir.get_next()
	if filename == "":
		print("Folder empty")
		return true
		#create_database_dialog.show()
	else:
		print("Folder NOT empty")
		return false
		#load_database()


func load_database():
	read_database_structure()
	update_database_viewer()


func open_db_config_file(permision):
	current_config_file_path = database_dir.get_current_dir() + "/" + DATABASE_CONFIG_FILE_NAME
	var config_file = File.new()
	config_file.open(current_config_file_path, permision)
	return config_file


func read_database_structure():
	var db_config_file = open_db_config_file(File.READ)
	create_new_database_structure()
	var file_content = db_config_file.get_as_text()
	print(file_content)
	if file_content != "":
		database_instance.godot_database = parse_json(file_content)
	db_config_file.close()


func save_database_structure():
	var db_config_file = open_db_config_file(File.WRITE)
	#for table_dict in database_instance.get_tables():
		#db_config_file.store_string(to_json(table_dict))
		#print(table_dict)
	db_config_file.store_string(to_json(database_instance.godot_database))
	db_config_file.close()


func update_database_viewer():
	update_tables_list_viewer()
	update_fields_list_viewer()


func update_fields_list_viewer():
	table_fields_container.clear()
	if len(database_instance.get_tables()) > 0:
		var current_table = database_instance.get_tables()[current_tables_list_index]
		for field in current_table["fields"]:
			table_fields_container.add_item(field)


func update_tables_list_viewer():
	tables_list_container.clear()
	for table in database_instance.get_tables():
		tables_list_container.add_item(table["t_name"])
		#var new_child_node = TABLE_NAME_LABEL.instance()
		#new_child_node.text = table["t_name"]
		#tables_list_container.add_child(new_child_node)


func _on_DatabasePathBrowseButton_button_up():
	file_dialog.show()


func _on_DatabasePathLineEdit_text_changed(new_text):
	if database_dir.open(new_text) == OK:
		print("Folder exists")
		#check_dirpath()
	else:
		print("Folder NOT exists")


func _on_CreateDatabaseDialog_confirmed():
	create_new_database_structure()
	create_db_config_file()
	read_database_structure()


func _on_AddNewTableButton_button_up():
	new_table_dialog.show()


func _on_NewTableDialog_confirmed():
	if database_instance == null:
		create_new_database_structure()
	database_instance.add_table(new_table_line_edit.text)
	update_database_viewer()


func _on_SaveDBButton_button_up():
	save_database_structure()


func _on_FileDialog_dir_selected(dir):
	database_dir = Directory.new()
	if database_dir.open(dir) == OK:
		print("Folder exists")
		database_path_lineedit.set_text(database_dir.get_current_dir())
		if is_dirpath_empty():
			create_database_dialog.show()
		else:
			load_database()


func _on_DeleteTableButton_button_up():
	database_instance.delete_table(current_tables_list_index)
	current_tables_list_index = -1
	update_database_viewer()


func _on_TablesListItemsList_item_selected(index):
	current_tables_list_index = index
	update_fields_list_viewer()


func _on_TableFieldsItemsList_item_selected(index):
	current_fields_list_index = index


func _on_BackButton_button_up():
	quit_dialog.show()


func _on_NewFieldDialog_confirmed():
	database_instance.add_field(new_field_line_edit.text, current_tables_list_index)
	update_database_viewer()


func _on_AddFieldButton_button_up():
	new_field_dialog.show()


func _on_DeleteFieldButton_button_up():
	database_instance.delete_field(current_tables_list_index, current_fields_list_index)
	current_fields_list_index = -1
	update_database_viewer()


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		_on_BackButton_button_up()


func _on_QuitDialog_confirmed():
	if autosave_check_box.pressed == true:
		save_database_structure()
	get_tree().quit()
