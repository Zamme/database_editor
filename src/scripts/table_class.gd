class_name GodotDBTable

var table_structure = {
	"t_name" : "",
	"fields" : []
}


func _init():
	print("Table created.")


# TODO: THESE TABLE AND FIELD METHODS ARE NOT IN USE!!!

func add_field(field_name : String):
	if field_name in table_structure["fields"]:
		print("Field exists. Adding cancelled.")
	else:
		table_structure["fields"].append(field_name)
		print("Field " + field_name + " added to table " + table_structure["t_name"])


func create_array():
	pass


func delete_field(field_name : String):
	if table_structure["fields"].has(field_name):
		table_structure["fields"].erase(field_name)
		print("Field " + field_name + " deleted from table " + table_structure["t_name"])
	else:
		print("Field not found")


func get_table_name():
	return table_structure["t_name"]


func set_table_name(table_name : String):
	if table_name != "":
		table_structure["t_name"] = table_name
	else:
		print("Blank name. Setting table name cancelled.")

