class_name GodotDB

var godot_database = {
	"db_name" : "godot_db",
	"tables" : []
}


func _init():
	print("Database created")


func add_field(field_name : String, table_index : int):
	if not field_name in godot_database["tables"][table_index]["fields"]:
		godot_database["tables"][table_index]["fields"].append(field_name)


func add_table(table_name : String):
	var all_tables_names = []
	for table in godot_database["tables"]:
		all_tables_names.append(table["t_name"])
	
	if (not table_name in all_tables_names):
		godot_database["tables"].append(create_table(table_name))
		print("Table " + table_name + " added.")
	else:
		print("Table " + table_name + " is already on DB.")
	
	print(table_name + " table_name")
	print(all_tables_names)


func create_table(table_name : String):
	var new_table : GodotDBTable = GodotDBTable.new()
	new_table.set_table_name(table_name)
	return new_table.table_structure


func delete_field(table_index : int, field_index : int):
	if field_index < len(godot_database["tables"][table_index]["fields"]):
		godot_database["tables"][table_index]["fields"].remove(field_index)


func delete_table(table_index : int):
	if table_index < len(godot_database["tables"]):
		godot_database["tables"].remove(table_index)
	else:
		print("Table not found.")


func get_tables():
	return godot_database["tables"]


func set_db_name(name : String):
	godot_database["db_name"] = name
